import kotlinx.coroutines.*

fun main(args: Array<String>) = runBlocking<Unit> {

    launch (Dispatchers.Default) {
        delay(1000)
        println("World!")
    }
    
    delay(2000)
    println("Hello, ")

}
