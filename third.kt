import kotlinx.coroutines.*

const val decorator = "///////////////////////////"

fun main(args: Array<String>) = runBlocking<Unit> {
    println(decorator)

    val job = launch {
        try {
            repeat(times = 3) { i ->
                println("I'm waiting $i ...")
                delay(1000)
            }

        } finally {
            println("I'm running finally")
            delay(1500)
        }
    }

    delay(2500)
    println("main: I'm tired of waiting!")

    job.join()

    println("main: Now I can quit")

    println(decorator)
}
