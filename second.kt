import kotlinx.coroutines.*

suspend fun firstNum(): Int {
    delay(1000)
    return 5
}

suspend fun secondNum(): Int {
    delay(1500)
    return 3
}

fun main() = runBlocking {

    val startTime = System.currentTimeMillis()

    val firstNum = firstNum()
    val secondNum = secondNum()

    val endTime = System.currentTimeMillis()

    println(firstNum + secondNum)
    println(endTime - startTime)

    // Async

    val asyncStartTime = System.currentTimeMillis()

    val asyncFirstNum = async { firstNum() }
    val asyncSecondNum = async { secondNum() }

    println(asyncFirstNum.await() + asyncSecondNum.await())
    val asyncEndTime = System.currentTimeMillis()
    println(asyncEndTime - asyncStartTime)
}
